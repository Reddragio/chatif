import controller.Controller;

/**
 * @author Jacques CHARNAY
 */
public class Main {

    public static void main(String[] args){
        Controller controller = new Controller();
        controller.initState();
    }

}
