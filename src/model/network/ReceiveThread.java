package model.network;

import controller.Controller;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Class to handle the message received from chat server
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class ReceiveThread extends Thread {
    private Controller controller;
    private BufferedReader socIn = null;

    private volatile boolean running = true;

    /**
     * terminate the receive thread
     */
    public void terminate() {
        running = false;
    }

    public ReceiveThread(Controller controller,BufferedReader socIn) {
        this.controller = controller;
        this.socIn = socIn;
    }

    /**
     * Wait for messages of chat server and handle them
     */
    public void run() {
        while(running){
            try {
                controller.receiveMessage(socIn.readLine());
            } catch (IOException e) {
                if(running){
                    controller.serverShutdown();
                }
            }
        }
    }

}