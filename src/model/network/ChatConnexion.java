package model.network;

import controller.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Class to handle the connexion with the chat server
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class ChatConnexion {
    private Controller controller;
    private Socket socket = null;
    private PrintStream socOut = null;
    private BufferedReader socIn = null;
    private ReceiveThread receiveThread = null;

    /**
     * Initialize the connection with the chat server
     * @param controller
     * @param address chat server address
     * @param port chat server port
     * @throws IOException
     */
    public ChatConnexion(Controller controller, String address, int port) throws IOException {
        this.controller = controller;

        // Connexion with server
        socket = new Socket(address,port);
        socIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        socOut= new PrintStream(socket.getOutputStream());

        //Thread to receive message:
        receiveThread = new ReceiveThread(controller,socIn);
        receiveThread.start();
    }

    /**
     * Close the connexion with the chat server
     */
    public void closeConnexion(){
        receiveThread.terminate();

        try {
            socOut.close();
            socIn.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * send a special message to server to indicate that we join the chat
     * @param pseudo
     */
    public void joinChat(String pseudo){
        socOut.println("j"+pseudo);
    }

    /**
     * send a message to server in order to add it into conversation
     * @param message
     */
    public void sendMessage(String message){
        socOut.println("m"+message);
    }

}
