package view;

import controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * @author Jacques CHARNAY
 */
public class MainWindow extends JFrame  implements ActionListener {
    private JEditorPane conversationBox;
    private JScrollPane messagesBoxWithScrollBar;
    private HTMLDocument HTMLConversation;
    private HTMLEditorKit editorKit;
    private JTextArea messageWritingArea;
    private JTextField pseudoField;
    private JTextField addressField;
    private JTextField portField;
    private JButton sendButton;
    private JButton connectButton;
    private JButton disconnectButton;

    private Controller controller;

    public MainWindow(){
        this.setTitle("Chat'IF");
        this.setSize(1050, 710);
        this.setLocationRelativeTo(null);
        JPanel chatPanel = new JPanel();

        conversationBox = new JEditorPane();
        conversationBox.setContentType("text/html");
        conversationBox.setEditable(false);
        editorKit = new HTMLEditorKit();
        conversationBox.setEditorKit(editorKit);
        HTMLConversation = new HTMLDocument();
        conversationBox.setDocument(HTMLConversation);

        DefaultCaret caret = (DefaultCaret)conversationBox.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        messagesBoxWithScrollBar = new JScrollPane(conversationBox);
        messagesBoxWithScrollBar.setPreferredSize(new Dimension(700, 500));
        messagesBoxWithScrollBar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JPanel messageWritingPanel = new JPanel();
        JPanel connexionPanel = new JPanel();
        connexionPanel.setMaximumSize(new Dimension(300,240));
        BoxLayout layoutConnexion = new BoxLayout(connexionPanel,BoxLayout.Y_AXIS);
        connexionPanel.setLayout(layoutConnexion);
        connexionPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 10, 10, 20),
                BorderFactory.createTitledBorder("Connexion")
        ));

        JLabel addressLabel = new JLabel();
        addressLabel.setText("Adresse du serveur:");
        addressField = new JTextField();
        addressField.setColumns(20);
        addressField.setMaximumSize(new Dimension(350,24));
        connexionPanel.add(addressLabel);
        connexionPanel.add(addressField);

        JLabel portLabel = new JLabel();
        portLabel.setText("Port:");
        portField = new JTextField();
        portField.setColumns(8);
        portField.setMaximumSize(new Dimension(350,24));
        connexionPanel.add(portLabel);
        connexionPanel.add(portField);

        JLabel pseudoLabel = new JLabel();
        pseudoLabel.setText("Pseudo:");
        pseudoField = new JTextField();
        pseudoField.setColumns(12);
        pseudoField.setMaximumSize(new Dimension(350,24));
        connexionPanel.add(pseudoLabel);
        connexionPanel.add(pseudoField);

        Component test = Box.createRigidArea(new Dimension(1,10));
        connexionPanel.add(test);
        connectButton = new JButton("Connexion");
        connectButton.addActionListener(this);
        connexionPanel.add(connectButton);
        Component test2 = Box.createRigidArea(new Dimension(1,10));
        connexionPanel.add(test2);
        disconnectButton = new JButton("Déconnexion");
        disconnectButton.addActionListener(this);
        connexionPanel.add(disconnectButton);
        Component test3 = Box.createRigidArea(new Dimension(1,10));
        connexionPanel.add(test3);

        messageWritingArea = new JTextArea(5, 45);
        messageWritingArea.setBounds(0, 0, 100, 10);
        messageWritingArea.setLineWrap(true);
        JScrollPane messageWritingWithScrollBar = new JScrollPane(messageWritingArea);
        messageWritingWithScrollBar.setBounds(0, 0, 100, 40);
        messageWritingWithScrollBar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        messageWritingPanel.add(messageWritingWithScrollBar,BorderLayout.CENTER);
        sendButton = new JButton("Envoyer");
        sendButton.addActionListener(this);
        messageWritingPanel.add(sendButton,BorderLayout.EAST);

        chatPanel.add(messagesBoxWithScrollBar, BorderLayout.CENTER);
        chatPanel.add(messageWritingPanel, BorderLayout.SOUTH);

        JPanel masterPanel = new JPanel();
        masterPanel.setLayout(new BoxLayout(masterPanel, BoxLayout.X_AXIS));
        JPanel connexionMasterPanel = new JPanel();
        connexionMasterPanel.setLayout(new BoxLayout(connexionMasterPanel, BoxLayout.Y_AXIS));
        connexionMasterPanel.add(connexionPanel);
        Component test4 = Box.createGlue();
        connexionMasterPanel.add(test4);
        masterPanel.add(connexionMasterPanel);
        masterPanel.add(chatPanel);

        //Default values:
        addressField.setText("localhost");
        portField.setText("1234");

        this.setContentPane(masterPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Add the message to the conversation
     * @param message
     */
    public void addMessageToConversation(String message){
        String newEntry = "<p>"+message+"</p>";

        try {
            editorKit.insertHTML(HTMLConversation, HTMLConversation.getLength(), newEntry, 1, 0, HTML.Tag.P);
        } catch (BadLocationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JScrollBar verticalScrollBar = messagesBoxWithScrollBar.getVerticalScrollBar();
        if(verticalScrollBar != null){
            verticalScrollBar.setValue(Integer.MAX_VALUE);
        }
    }

    public void setSendButtonState(boolean enable){
        sendButton.setEnabled(enable);
    }

    public void setConnectButtonState(boolean enable){
        connectButton.setEnabled(enable);
    }

    public void setDisconnectButtonState(boolean enable){
        disconnectButton.setEnabled(enable);
    }

    public void setAddressFieldEnable(boolean enable) {
        addressField.setEnabled(enable);
    }

    public void setPortFieldEnable(boolean enable) {
        portField.setEnabled(enable);
    }

    public void setPseudoFieldEnable(boolean enable) {
        pseudoField.setEnabled(enable);
    }

    public void clearWritingArea(){
        messageWritingArea.setText("");
    }

    public void clearConversationArea(){
        conversationBox.setText("");
    }

    /**
     * We gather the events linked to buttons and execute the associated actions
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==sendButton) {
            controller.sendMessage(messageWritingArea.getText());
        }
        else if(e.getSource()==connectButton){
            controller.connect(addressField.getText(),portField.getText(),pseudoField.getText());
        }
        else if(e.getSource()==disconnectButton){
            controller.disconnect(pseudoField.getText());
        }
    }
}