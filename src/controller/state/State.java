package controller.state;

import controller.Controller;
import model.network.ChatConnexion;
import view.MainWindow;

/**
 * Interface of states of the application
 * @author Jacques CHARNAY
 */
public interface State {

    default void enter(Controller controller, MainWindow mainWindow) {
    }

    default void sendMessage(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion, String message){
    }

    default void receiveMessage(Controller controller, MainWindow mainWindow, String message){
    }

    default void connect(Controller controller, MainWindow mainWindow, String address, String port, String pseudo){
    }

    default void disconnect(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion, String pseudo){
    }

    default void serverShutdown(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion){

    }

}
