package controller.state;

import controller.Controller;
import model.network.ChatConnexion;
import view.MainWindow;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * State when the application is not connected to a chat server
 * @author Jacques CHARNAY
 */
public class Offline implements State{

    @Override
    public void enter(Controller controller, MainWindow mainWindow) {
        mainWindow.setSendButtonState(false);
        mainWindow.setConnectButtonState(true);
        mainWindow.setDisconnectButtonState(false);

        mainWindow.setAddressFieldEnable(true);
        mainWindow.setPortFieldEnable(true);
        mainWindow.setPseudoFieldEnable(true);
    }

    /**
     * Connection to the chat server
     * @param controller
     * @param mainWindow
     * @param address
     * @param port
     * @param pseudo
     */
    @Override
    public void connect(Controller controller, MainWindow mainWindow, String address, String port, String pseudo){
        mainWindow.clearConversationArea();

        if(pseudo.length() == 0){
            mainWindow.addMessageToConversation("ERREUR - Le pseudo doit au moins comporter un caractère");
            return;
        }

        ChatConnexion newChatConnexion;
        try{
            newChatConnexion = new ChatConnexion(controller,address,Integer.parseInt(port));
        } catch (UnknownHostException e) {
            mainWindow.addMessageToConversation("<font color=\"red\">Erreur - Serveur inconnu: " + address+"</font>");
            return;
        } catch(NumberFormatException e){
            mainWindow.addMessageToConversation("<font color=\"red\">Erreur - Le port doit être un entier</font>");
            return;
        }
        catch (IOException e) {
            mainWindow.addMessageToConversation("<font color=\"red\">Erreur - Impossible de récuperer les flux d'entrée/sortie pour la connexion</font>");
            return;
        }

        controller.changeState(SingletonStates.online);
        controller.setChatConnexion(newChatConnexion);
        newChatConnexion.joinChat(pseudo);
    }

}
