package controller.state;

/**
 * Singletons to access each state
 * @author Jacques CHARNAY
 */
public class SingletonStates {
    public static Offline offline = new Offline();
    public static Online online = new Online();
}
