package controller.state;

import controller.Controller;
import model.network.ChatConnexion;
import view.MainWindow;

/**
 * State when the application is actually connected to a chat server
 * @author Jacques CHARNAY
 */
public class Online implements State{

    @Override
    public void enter(Controller controller, MainWindow mainWindow) {
        mainWindow.setSendButtonState(true);
        mainWindow.setConnectButtonState(false);
        mainWindow.setDisconnectButtonState(true);

        mainWindow.setAddressFieldEnable(false);
        mainWindow.setPortFieldEnable(false);
        mainWindow.setPseudoFieldEnable(false);
    }

    /**
     * Send a message
     * @param controller
     * @param mainWindow
     * @param chatconnexion
     * @param message
     */
    @Override
    public void sendMessage(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion, String message){
        mainWindow.clearWritingArea();
        chatconnexion.sendMessage(message);
    }

    /**
     * Receive a message from chat server
     * @param controller
     * @param mainWindow
     * @param message
     */
    @Override
    public void receiveMessage(Controller controller, MainWindow mainWindow, String message){
        mainWindow.addMessageToConversation(message);
    }

    /**
     * Disconnect from chat server
     * @param controller
     * @param mainWindow
     * @param chatconnexion
     * @param pseudo
     */
    @Override
    public void disconnect(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion, String pseudo){
        chatconnexion.closeConnexion();
        controller.setChatConnexion(null);
        controller.changeState(SingletonStates.offline);
    }

    /**
     * Event triggered when server shutdown
     * @param controller
     * @param mainWindow
     * @param chatconnexion
     */
    @Override
    public void serverShutdown(Controller controller, MainWindow mainWindow, ChatConnexion chatconnexion){
        chatconnexion.closeConnexion();
        controller.setChatConnexion(null);
        controller.changeState(SingletonStates.offline);

        mainWindow.addMessageToConversation("<font color=\"red\">ERREUR: La connexion avec le serveur a été perdue</font>");
    }

}
