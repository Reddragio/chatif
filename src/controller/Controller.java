package controller;

import controller.state.SingletonStates;
import controller.state.State;
import model.network.ChatConnexion;
import view.MainWindow;

/**
 * Controller of the application
 * @author Jacques CHARNAY
 */
public class Controller {
    private MainWindow mainWindow;
    private State currentState;
    private ChatConnexion chatConnexion;

    public Controller(){
        mainWindow = new MainWindow();
    }

    public void setChatConnexion(ChatConnexion chatConnexion) {
        this.chatConnexion = chatConnexion;
    }

    public void initState(){
        currentState = SingletonStates.offline;
        currentState.enter(this,mainWindow);

        mainWindow.setController(this);
    }

    /**
     * Change the current state of the application
     * @param nextState
     */
    public void changeState(State nextState) {
        currentState = nextState;
        currentState.enter(this,mainWindow);
    }

    /**
     * Connection to the chat server
     * @param address
     * @param port
     * @param pseudo
     */
    public void connect(String address, String port, String pseudo){
        currentState.connect(this,mainWindow,address,port,pseudo);
    }

    /**
     * disconnection from the chat server
     * @param pseudo
     */
    public void disconnect(String pseudo){
        currentState.disconnect(this,mainWindow,chatConnexion,pseudo);
    }

    /**
     * Send a message to chat
     * @param message
     */
    public void sendMessage(String message){
        currentState.sendMessage(this,mainWindow,chatConnexion,message);
    }

    /**
     * Receive message from server
     * @param message
     */
    public void receiveMessage(String message){
        currentState.receiveMessage(this,mainWindow,message);
    }

    /**
     * Event triggered went the server shutdown
     */
    public void serverShutdown(){
        currentState.serverShutdown(this,mainWindow,chatConnexion);
    }

}
